<?php

declare(strict_types=1);

namespace Paneric\Doctrine;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\UnderscoreNamingStrategy;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Cache\FilesystemCache;
use Doctrine\ORM\EntityManager;

class EntityManagerBuilder
{
    public function build(array $settings): EntityManagerInterface
    {
        try {
            $config = Setup::createAnnotationMetadataConfiguration(
                $settings['paths'],
                $settings['is_dev_mode'],
                $settings['proxy_dir'],
                $settings['cache'],
                $settings['use_simple_annotation_reader']
            );

            $config->setNamingStrategy(
                new UnderscoreNamingStrategy(CASE_LOWER)
            );

            $config->setMetadataDriverImpl(
                new AnnotationDriver(
                    new AnnotationReader,
                    $settings['paths']
                )
            );

            $config->setMetadataCacheImpl(
                new FilesystemCache(
                    $settings['cache_dir']
                )
            );

            return EntityManager::create(
                $settings['connection'],
                $config
            );
        } catch (ORMException $e) {
            $e->getMessage();
        }

        return null;
    }
}
