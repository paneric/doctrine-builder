# Doctrine Builder

Another one, tiny and handy instance builder, this time for Doctrine Entity Manager.

## Required

* PHP 7.1+

## File structure
* **doctrine-builder**
    * **src**
        * EntityManagerBuilder.php

### composer
```sh
$ composer require paneric/doctrine-builder:1.0.0
```

## Integration

Slim Framework example (ver.4)

### configuration
Below, there is presented default configuration/settings pattern required for a proper integration.

*settings.php (for a sake of simplicity let's say in your root folder)*
```php
return [
    'doctrine_orm' => [
        // you should add any other path containing annotated entity classes
        'paths' => [
            APP_ROOT.'/src/Entity',
        ],

        // if true, metadata caching is forcefully disabled
        'is_dev_mode' => true,

        'proxy_dir' => null,

        'cache' => null,

        'use_simple_annotation_reader' => false,

        // path where the compiled metadata info will be cached
        // make sure the path exists and it is writable
        'cache_dir' => APP_ROOT.'/var/cache/doctrine',

        'connection' => [
            'driver' => 'pdo_mysql',
            'host' => '127.0.0.1',
            'port' => 3306,
            'dbname' => 'doctrine',
            'user' => 'root',
            'password' => 'root',
            'charset' => 'UTF8'
        ],
    ]
];
```

### front controller

Once again some ugly (just for a demonstration purpose) example of integration within a front controller. However, 
first you need to prepare some bootstrap for the entity manager.

It is a handy way for both, an application and doctrine cli to instantiate the entity manager for their own 
purposes. It is accomplished apart from the container, through a separate script accessible for both of them. 
Instantiation of entity manager exclusively through dependency definitions - let's just say - makes above requirements 
a bit problematic. That is why container's method "set" is performed within our front controller.

*doctrine-orm.php (for a sake of simplicity let's say in your root folder)*
```php
declare(strict_types=1);

use Paneric\Doctrine\EntityManagerBuilder;

$entityManagerBuilder = new EntityManagerBuilder();

return $entityManagerBuilder->build(
    (require ROOT_FOLDER . 'settings.php')['doctrine_orm']
);
```
Then, in your front controller:

*index.php*
```php
use DI\ContainerBuilder;
use Slim\Factory\AppFactory;
use Paneric\Doctrine\EntityManagerBuilder;
use DI\Container;

define('ENV', 'dev');

define('ROOT_FOLDER', dirname(__DIR__) . '/');
define('APP_FOLDER', ROOT_FOLDER . 'src/');

require ROOT_FOLDER . 'vendor/autoload.php';

try{
    $settings = ...;

    $builder = new ContainerBuilder();
    $builder->addDefinitions(['some_definition']); // add some definitions
    $container = $builder->build();

    AppFactory::setContainer($container);
    $app = AppFactory::create();
    
    $container->set(
        'entity_manager',
        require ROOT_FOLDER.'doctrine_orm.php'
    );

    $app->run();
} catch (Exception $e) {
   echo $e->getMessage();
}
```

### doctrine console

There is one more important thing that can not be forgotten. Doctrine console requires some tiny support of the file
named exactly **cli-config.php** located alternatively in your **ROOT_FOLDER** or **ROOT_FOLDER/config/**. This file may
look like as below:

*cli-config.php (for a sake of simplicity let's say in your root folder)*

```php
declare(strict_types=1);

use Doctrine\ORM\Tools\Console\ConsoleRunner;

define('ROOT_FOLDER', dirname(__DIR__) . '/');

require ROOT_FOLDER.'vendor/autoload.php';

return ConsoleRunner::createHelperSet(
    require ROOT_FOLDER.'doctrine_orm.php'
);
```
