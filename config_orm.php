<?php

return [
    // you should add any other path containing annotated entity classes
    'paths' => [
        APP_ROOT.'/src/Entity',
    ],

    // if true, metadata caching is forcefully disabled
    'is_dev_mode' => true,

    'proxy_dir' => null,

    'cache' => null,

    'use_simple_annotation_reader' => false,

    // path where the compiled metadata info will be cached
    // make sure the path exists and it is writable
    'cache_dir' => APP_ROOT.'/var/cache/doctrine',

    'connection' => [
        'driver' => 'pdo_mysql',
        'host' => '127.0.0.1',
        'port' => 3306,
        'dbname' => 'doctrine',
        'user' => 'root',
        'password' => 'root',
        'charset' => 'UTF8'
    ],
];
