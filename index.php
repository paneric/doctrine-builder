<?php

declare(strict_types=1);

use Paneric\Doctrine\EntityManagerBuilder;

define('APP_ROOT', __DIR__);

require APP_ROOT . 'vendor/autoload.php';

$EntityManagerBuilder = new EntityManagerBuilder(
    require_once('config_orm.php')
);

$entityManager = $EntityManagerBuilder->build();
