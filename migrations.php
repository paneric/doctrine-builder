<?php // File in the root of a project

return [
    'name'                    => '', // Project's migrations name
    'migrations_namespace'    => '', // Namespace of migrations class within a project
    'table_name'              => 'doctrine_migration_versions',
    'column_name'             => 'version',
    'column_length'           => 14,
    'executed_at_column_name' => 'executed_at',
    'migrations_directory'    => '', // Folder of migrations class within a project
    'all_or_nothing'          => true,
    'check_database_platform' => true,
];
